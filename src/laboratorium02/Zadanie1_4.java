package laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1_4 {
    public static void main(String [] args)
    {
        Scanner wczyt = new Scanner(System.in);
        int n = 0;
        System.out.println("Podaj rozmiar tablicy: ");
        int probka = wczyt.nextInt();
        if (probka >= 1 && probka <= 100) {
            n = probka;
        }
        int[] tablica = new int[n];
        Random generuj=new Random();
        for (int i = 0; i < tablica.length; i++)
        {

            tablica[i] =generuj.nextInt(1999)-999;

        }
        int dodatnie = 0;
        int ujemne = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] > 0) {
                dodatnie += tablica[i];
            } else if (tablica[i] < 0) {
                ujemne += tablica[i];
            }
        }
        System.out.println("Suma liczb dodatnich: " + dodatnie + " suma liczb ujemnych: " + ujemne);
    }
}
