package laboratorium02;

import java.util.*;
public class Zadanie2_2 {
    public static int ileDodatnich(int[]tablica)
    {
        int wynik=0;
        for(int i=0;i<tablica.length;i++)
        {
            if(tablica[i]>0)
            {
                wynik++;
            }
        }
        return wynik;
    }
    public static int ileUjemnych(int [] tablica)
    {
        int wynik=0;
        for(int i=0;i<tablica.length;i++)
        {
            if(tablica[i]<0)
            {
                wynik++;
            }
        }
        return wynik;
    }
    public static int ileZer(int [] tablica)
    {
        int wynik=0;
        for(int i=0 ;i<tablica.length;i++)
        {
            if(tablica[i]==0)
            {
                wynik++;
            }
        }
        return wynik;
    }
    public static void main(String[] args)
    {
        Scanner wczyt = new Scanner(System.in);
        System.out.println("Podaj rozmiar tablicy(od 1 do 100): ");
        int n=wczyt.nextInt();
        int[] tablica=new int[n];
        System.out.println("Max: ");
        int max=wczyt.nextInt();
        System.out.println("Min: ");
        int min=wczyt.nextInt();
        Zadanie2_1.generuj(tablica,n,min,max);
        System.out.println(Arrays.toString(tablica));
        System.out.println(ileDodatnich(tablica));
        System.out.println(ileUjemnych(tablica));
        System.out.println(ileZer(tablica));
    }
}
