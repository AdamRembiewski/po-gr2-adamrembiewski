package laboratorium02;

import java.util.Arrays;
import java.util.Scanner;

public class Zadanie2_7 {
    public static void odwrocFragment(int [] tablica, int lewy, int prawy)
    {
        if(prawy>=1&&lewy>=1&&prawy<tablica.length)
        {
            while (prawy > lewy)
            {
                int tmp = tablica[lewy-1];
                tablica[lewy-1] = tablica[prawy-1];
                tablica[prawy-1] = tmp;
                lewy++;
                prawy--;
            }
        }
    }
    public static void main(String[] args)
    {
        Scanner wczyt = new Scanner(System.in);
        System.out.println("Podaj rozmiar tablicy(od 1 do 100): ");
        int n=wczyt.nextInt();
        int[] tablica=new int[n];
        System.out.println("Max: ");
        int max=wczyt.nextInt();
        System.out.println("Min: ");
        int min=wczyt.nextInt();
        Zadanie2_1.generuj(tablica,n,min,max);
        System.out.println(Arrays.toString(tablica));
        System.out.println("Podaj lewą granice: ");
        int lewy=wczyt.nextInt();
        System.out.println("Podaj prawą granice: ");
        int prawy=wczyt.nextInt();
        odwrocFragment(tablica,lewy,prawy);
        System.out.println(Arrays.toString(tablica));
    }
}
