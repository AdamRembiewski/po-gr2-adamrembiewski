package  pl.edu.uwm.wmii.adamrembiewski.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1_2 {
    public static void main(String[] args)
    {
        Scanner wczyt = new Scanner(System.in);
        int n = 0;
        System.out.println("Podaj rozmiar tablicy: ");
        int probka = wczyt.nextInt();
        if (probka >= 1 && probka <= 100) {
            n = probka;
        }
        int[] tablica = new int[n];
        Random generuj=new Random();
        for (int i = 0; i < tablica.length; i++)
        {

            tablica[i] =generuj.nextInt(1999)-999;

        }
        int dodatnie = 0;
        int ujemne = 0;
        int zera = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] > 0) {
                dodatnie++;
            }
            else if (tablica[i] < 0) {
                ujemne++;
            } else {
                zera++;
            }
        }
        System.out.println("Ilosc liczb dodatnich: " + dodatnie + " ilosc liczb ujemnych: " + ujemne + " ilość zer: " + zera);
    }
}

