package  pl.edu.uwm.wmii.adamrembiewski.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1_3 {
    public static void main(String[] args)
    {
        Scanner wczyt = new Scanner(System.in);
        int n = 0;
        System.out.println("Podaj rozmiar tablicy: ");
        int probka = wczyt.nextInt();
        if (probka >= 1 && probka <= 100) {
            n = probka;
        }
        int[] tablica = new int[n];
        Random generuj=new Random();
        for (int i = 0; i < tablica.length; i++)
        {

            tablica[i] =generuj.nextInt(1999)-999;

        }
        int najwieksza = tablica[0];
        int wynik = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] > najwieksza) {
                najwieksza = tablica[i];
            }
        }
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] == najwieksza) {
                wynik++;
            }
        }

        System.out.println("Element najwiekszy to :" + najwieksza + " wystepuje: " + wynik);
    }
}