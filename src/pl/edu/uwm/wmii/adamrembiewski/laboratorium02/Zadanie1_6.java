package  pl.edu.uwm.wmii.adamrembiewski.laboratorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadanie1_6 {
    public static void main(String[] args)
    {
        Scanner wczyt = new Scanner(System.in);
        int n = 0;
        System.out.println("Podaj rozmiar tablicy: ");
        int probka = wczyt.nextInt();
        if (probka >= 1 && probka <= 100) {
            n = probka;
        }
        int[] tablica = new int[n];
        Random generuj=new Random();
        for (int i = 0; i < tablica.length; i++)
        {

            tablica[i] =generuj.nextInt(1999)-999;

        }
        for(int i=0;i<tablica.length;i++)
        {
            if(tablica[i]>0)
            {
                tablica[i]=1;
            }
            else if(tablica[i]<0)
            {
                tablica[i] = -1;
            }
        }
        System.out.println(Arrays.toString(tablica));

    }
}
