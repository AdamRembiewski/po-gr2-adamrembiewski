package  pl.edu.uwm.wmii.adamrembiewski.laboratorium02;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Zadanie1_5 {
    public static void main(String[] args)
    {
        Scanner wczyt = new Scanner(System.in);
        int n = 0;
        System.out.println("Podaj rozmiar tablicy: ");
        int probka = wczyt.nextInt();
        if (probka >= 1 && probka <= 100) {
            n = probka;
        }
        int[] tablica = new int[n];
        Random generuj=new Random();
        for (int i = 0; i < tablica.length; i++)
        {

            tablica[i] =generuj.nextInt(1999)-999;

        }
        int wynik = 0;
        List<Integer> pomoc = new ArrayList<Integer>();
        pomoc.add(0);
        int maks = pomoc.get(0);
        for (int i = 0; i < tablica.length; i++)
        {
            if (tablica[i] > 0)
            {
                wynik++;
                if(i==tablica.length-1)
                {
                    pomoc.add(wynik);
                }
            }
            else if(tablica[i]<=0)
            {
                pomoc.add(wynik);
                wynik=0;
            }
        }
        for (int i = 0; i < pomoc.size(); i++) {
            if (maks < pomoc.get(i)) {
                maks = pomoc.get(i);
            }
        }



        System.out.println("Długośc fragmentu dodatniego to: " + maks);
    }
}
