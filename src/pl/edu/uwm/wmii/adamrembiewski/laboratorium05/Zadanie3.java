package pl.edu.uwm.wmii.adamrembiewski.laboratorium05;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie3 {
    static ArrayList<Integer>reversed(ArrayList<Integer>a)
    {
        ArrayList<Integer>wynik=new ArrayList<Integer>();
        for(int i=0;i<a.size();i++)
        {
            wynik.add(0);
        }
        for(int i=0;i<wynik.size();i++)
        {
            wynik.set(i,a.get(a.size()-1-i));
        }
        return wynik;
    }
    static void reverse(ArrayList<Integer>a)
    {
        int tmp=0;
        for(int i=0;i<(a.size()/2)-1;i++)
        {
            tmp=a.get(i);
            a.set(i,a.get(a.size()-1-i));
            a.set(a.size()-1-i,tmp);

        }
        System.out.println(a);
    }
    public static void main(String[]args)
    {
        ArrayList<Integer>a=new ArrayList<Integer>(Arrays.asList(33,261,11,2,3,4,5,9,22,212));
        System.out.println(reversed(a));
        reverse(a);
    }
}