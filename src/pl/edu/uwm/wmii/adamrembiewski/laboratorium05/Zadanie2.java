package pl.edu.uwm.wmii.adamrembiewski.laboratorium05;

import sun.invoke.empty.Empty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Zadanie2 {
    static ArrayList<Integer> merge(ArrayList<Integer>a,ArrayList<Integer>b) {
        ArrayList <Integer> wynik = new ArrayList <Integer>();
        ArrayList <Integer> tmp = a;
        ArrayList <Integer> tmp2 = b;
        if(a.size()<b.size())
        {
            tmp=b;
            tmp2=a;
        }
        int i=0;
        while(i!=tmp2.size())
        {
            wynik.add(tmp.get(i));
            wynik.add(tmp2.get(i));
            i++;
        }
        while (i != tmp.size()) {
            wynik.add(tmp.get(i));
            i++;
        }

        return wynik;
    }
    static ArrayList<Integer>mergeSort(ArrayList<Integer>a,ArrayList<Integer>b)
    {
        ArrayList<Integer>wieksza=a;
        ArrayList<Integer>mniejsza=b;

        if(a.size()<b.size())
        {
            mniejsza=a;
            wieksza=b;
        }
        Collections.sort(wieksza);
        Collections.sort(mniejsza);
        ArrayList<Integer>wynik=new ArrayList <Integer>();

        if(!mniejsza.isEmpty()||!wieksza.isEmpty()) {
            while(!mniejsza.isEmpty()&&!wieksza.isEmpty()) {
                if (mniejsza.get(0) > wieksza.get(0)) {
                    wynik.add(wieksza.get(0));
                    wieksza.remove(0);
                } else if (mniejsza.get(0) < wieksza.get(0)) {
                    wynik.add(mniejsza.get(0));
                    mniejsza.remove(0);
                }
            }
        }
        int i=0;
        int j=0;
        if(mniejsza.isEmpty()) {
            while (i != wieksza.size()) {
                wynik.add(wieksza.get(i));
                i++;
            }
        }

        else if(wieksza.isEmpty()) {
            while (j != mniejsza.size()) {
                wynik.add(mniejsza.get(j));
                j++;
            }
        }
        return wynik;
    }
    public static void main(String[] args)
    {
        ArrayList<Integer>a=new ArrayList <Integer>(Arrays.asList(2,17,54,73,91,130));
        ArrayList<Integer>b=new ArrayList <Integer>(Arrays.asList(7,35,67,38,69,112,-1));
        System.out.println(merge(a,b));
        System.out.println(mergeSort(a,b));
    }
}

