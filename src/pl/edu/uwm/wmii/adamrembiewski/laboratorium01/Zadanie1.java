package  pl.edu.uwm.wmii.adamrembiewski.laboratorium01;

import java.util.*;

class dzialania {
    static int silnia(int n) {
        if (n < 0) {
            return 0;
        } else if (n <= 1) {
            return 1;

        } else {
            return n * silnia(n - 1);
        }

    }

    static int dodawanieciagow(int n) {
        int wynik = 0;
        Scanner in=new Scanner(System.in);

        for (int i = 0; i < n; i++) {
            System.out.printf("Podaj %d wyraz ciągu: %n",i+1);
            int wczyt=in.nextInt();
            wynik = wynik + wczyt;
        }
        System.out.print("Twoj wynik to: ");
        return wynik;
    }

    static int mnozenieciagow(int n) {
        int wynik = 1;
        Scanner in=new Scanner(System.in);
        for (int i = 0; i < n; i++) {
            System.out.printf("Podaj %d wyraz ciągu: %n",i+1);
            int wczyt=in.nextInt();
            wynik = wynik * wczyt;
        }
        System.out.print("Twoj wynik to: ");
        return wynik;
    }

    static int dodawaniebezwzgledna(int n) {
        int wynik = 0;
        Scanner in=new Scanner(System.in);
        for (int i = 0; i < n; i++) {
            System.out.printf("Podaj %d wyraz ciągu: %n",i+1);
            int wczyt=in.nextInt();
            wynik = wynik + Math.abs(wczyt);
        }
        System.out.print("Twoj wynik to: ");
        return wynik;
    }

    static double pierwiastekzbezwglednej(int n) {
        double wynik = 0;
        Scanner in=new Scanner(System.in);
        for (int i = 0; i < n; i++) {
            System.out.printf("Podaj %d wyraz ciągu: %n",i+1);
            int wczyt=in.nextInt();
            wynik = wynik + (Math.sqrt(Math.abs(wczyt)));
        }
        System.out.print("Twoj wynik to: ");
        return wynik;
    }

    static int mnozbezwgzledna(int n) {
        int wynik = 1;
        Scanner in=new Scanner(System.in);
        for (int i = 0; i < n; i++)
        {
            System.out.printf("Podaj %d wyraz ciągu: %n",i+1);
            int wczyt=in.nextInt();
            wynik = wynik * Math.abs(wczyt);
        }
        System.out.print("Twoj wynik to: ");
        return wynik;
    }

    static double potegadodaj(int n) {
        double wynik = 0;
        Scanner in=new Scanner(System.in);
        for (int i = 0; i < n; i++)
        {
            System.out.printf("Podaj %d wyraz ciągu: %n",i+1);
            int wczyt=in.nextInt();
            wynik = wynik + Math.pow(wczyt, 2);
        }
        System.out.print("Twoj wynik to: ");
        return wynik;
    }

    static int dodajiodejmij(int n) {
        int wynik = 0;
        Scanner in=new Scanner(System.in);

        for (int i = 1; i <= n; i++) {
            System.out.printf("Podaj %d wyraz ciągu: %n",i);
            int wczyt=in.nextInt();
            if (i % 2 == 0)
            {
                wynik = wynik + wczyt;
            }
            else
            {
                wynik = wynik - wczyt;
            }
        }
        System.out.print("Twoj wynik to: ");
        return wynik + ((int) Math.pow(-1, n + 1) * n);
    }

    static float dodajiodejmijzsilnia(int n) {
        float wynik = 0;
        Scanner in=new Scanner(System.in);

        for (int i = 1; i <= n; i++) {
            System.out.printf("Podaj %d wyraz ciągu: %n",i);

            int wczyt=in.nextInt();
            if (i % 2 == 0)
            {

                wynik = wynik + (wczyt/ silnia(i));
            }
            else
            {
                wynik = wynik - (wczyt / silnia(i));
            }
        }
        System.out.print("Twoj wynik to: ");
        return wynik+ (((float) Math.pow(-1, n + 1) * n) / silnia(n));
    }
    static void wyswietl(int n)
    {
        System.out.println(dodawanieciagow(n));//ok
        System.out.println(mnozenieciagow(n));//ok
        System.out.println(dodawaniebezwzgledna(n));//ok
        System.out.println(pierwiastekzbezwglednej(n));//ok
        System.out.println(mnozbezwgzledna(n));//ok
        System.out.println(potegadodaj(n));//ok
        System.out.println(dodajiodejmij(n));//ok
        System.out.println(dodajiodejmijzsilnia(n));//ok
    }
}

public class Zadanie1 {

    public static void main(String[] args) {
        Scanner wejscie = new Scanner(System.in);
        System.out.println("Podaj n");
        int przykładowa = wejscie.nextInt();
        dzialania.wyswietl(przykładowa);
        //Zadanie 1.2
        System.out.println("Podaj wielkość ciągu:");
        int wartoscndo2=wejscie.nextInt();
        List<Integer>lista=new ArrayList<Integer>();
        for(int i=0;i<wartoscndo2;i++)
        {
            System.out.printf("Podaj %d element ciągu", i+1);
            int dodaj=wejscie.nextInt();
            lista.add(dodaj);
        }
        int tmp=lista.get(0);
        lista.remove(0);
        lista.add(tmp);
        for(int i=0;i<wartoscndo2;i++)
        {
            System.out.printf("%d",lista.get(i));

        }
    }
}