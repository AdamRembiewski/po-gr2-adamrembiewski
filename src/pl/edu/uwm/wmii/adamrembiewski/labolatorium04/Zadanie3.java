package pl.edu.uwm.wmii.adamrembiewski.labolatorium04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie3 {
    public static void main(String []args)
    {
        if (args.length!=2)
        {
            System.err.println("Zła ilość argumentów");
            System.exit(1);
        }

        File cos = new File(args[0]);
        int ileWystapien=0;
        try{
            Scanner wczyt = new Scanner(cos);

            while(wczyt.hasNextLine())
            {
                String str=wczyt.nextLine();
                ileWystapien+=Zadanie1.countSubStr(str,args[1]);
            }
            wczyt.close();

        }
        catch(FileNotFoundException e)
        {
            System.err.println(cos.getPath()+" nie istnieje");
        }
        System.out.println(ileWystapien);
    }
}