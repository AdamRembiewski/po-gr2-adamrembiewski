package pl.edu.uwm.wmii.adamrembiewski.labolatorium06;

public class RachunekBankowyTest {
    public static void main(String[]args)
    {
        RachunekBankowy saver1=new RachunekBankowy(2000);
        RachunekBankowy saver2=new RachunekBankowy(3000);
        saver1.setRocznaStopaProcentowa(4);
        saver2.setRocznaStopaProcentowa(4);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        saver1.setRocznaStopaProcentowa(5);
        saver2.setRocznaStopaProcentowa(5);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
    }
}