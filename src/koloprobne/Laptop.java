package koloprobne;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;

public class Laptop extends Komputer implements  Cloneable, Comparable<Komputer> {
    public Laptop(String sName, LocalDate sDataProdukcji, Boolean sCzyAple) {
        super(sName, sDataProdukcji);
        setCzyApple(sCzyAple);
    }

    public Boolean getCzyApple() {
        return czyApple;
    }

    public void setCzyApple(Boolean czyApple) {
        this.czyApple = czyApple;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Laptop temp = new Laptop(this.getNazwa(), this.getDataProdukcji(), this.getCzyApple());
        return temp;
    }

    @Override
    public boolean equals(Object obj) {
        Laptop temp = (Laptop) obj;
        return (super.equals(obj) && this.getCzyApple() == temp.getCzyApple());
    }

    @Override
    public int compareTo(@NotNull Komputer o) {
        Laptop temp = (Laptop)o;
        int x = super.compareTo(o);

        if (x == 0) {
            return this.czyApple.compareTo(temp.czyApple);
        }

        return x;
    }

    @Override
    public String toString() {
        return super.toString() + " " + this.getCzyApple().toString();
    }

    private Boolean czyApple;
}