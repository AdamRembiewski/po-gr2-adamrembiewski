package koloprobne;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        ArrayList<Komputer> grupa = new ArrayList<Komputer>();

        grupa.add(new Komputer("ca", LocalDate.of(1999, 12, 01)));
        grupa.add(new Komputer("as", LocalDate.of(1998, 12, 01)));
        grupa.add(new Komputer("zx", LocalDate.of(2999, 12, 01)));
        grupa.add(new Komputer("xa4", LocalDate.of(1999, 12, 01)));
        grupa.add(new Komputer("as", LocalDate.of(1990, 12, 01)));

        for (Komputer k: grupa) {
            System.out.println(k.toString());
        }

        System.out.println();

        grupa.sort(Komputer::compareTo);

        for (Komputer k: grupa) {
            System.out.println(k.toString());
        }

        System.out.println("-------------------------");

        ArrayList<Laptop> grupaLaptopow = new ArrayList<Laptop>();

        grupaLaptopow.add(new Laptop("ae", LocalDate.of(1999, 12, 01), true));
        grupaLaptopow.add(new Laptop("ass", LocalDate.of(1998, 12, 01), false));
        grupaLaptopow.add(new Laptop("ae", LocalDate.of(2999, 12, 01), false));
        grupaLaptopow.add(new Laptop("za", LocalDate.of(1999, 12, 01), false));
        grupaLaptopow.add(new Laptop("ae", LocalDate.of(1999, 12, 01), false));

        for (Laptop k: grupaLaptopow) {
            System.out.println(k.toString());
        }

        System.out.println();

        grupaLaptopow.sort(Laptop::compareTo);

        for (Laptop k: grupaLaptopow) {
            System.out.println(k.toString());
        }
    }
}
