package koloprobne;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;

public class Komputer implements Cloneable, Comparable<Komputer> {
    public Komputer(String sNazwa, LocalDate sDataProdukcji) {
        setNazwa(sNazwa);
        setDataProdukcji(sDataProdukcji);
    }

    public String getNazwa() {
        return nazwa;
    }

    public LocalDate getDataProdukcji() {
        return dataProdukcji;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setDataProdukcji(LocalDate dataProdukcji) {
        this.dataProdukcji = dataProdukcji;
    }

    @Override
    public boolean equals(Object obj) {
        Komputer temp = (Komputer) obj;
        if (this.nazwa.equals(temp.nazwa) && this.dataProdukcji.equals(temp.dataProdukcji)) {
            return true;
        }

        return false;
    }

    @Override
    public int compareTo(@NotNull Komputer o) {
        int temp = this.nazwa.compareTo(o.nazwa);

        if (temp == 0) {
            return this.dataProdukcji.compareTo(o.dataProdukcji);
        }

        return temp;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Komputer temp = new Komputer(this.getNazwa(), this.getDataProdukcji());
        return temp;
    }

    @Override
    public String toString() {
        return this.getNazwa()+ " " + this.getDataProdukcji().toString();
    }

    private String nazwa;
    private LocalDate dataProdukcji;
}
